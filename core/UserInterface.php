<?php

namespace Core;

interface UserInterface {
    
    public function getPseudo();
    public function getPassword();
    public function getRole_id();
}
